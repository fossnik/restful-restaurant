const webpack = require("webpack");
const path = require("path");

module.exports = {
	entry: {
		index: [
			"webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000",
			"./src/index.js"
		]
	},
	output: {
		path: path.resolve(__dirname, "./public"),
		filename: "bundle.js",
		publicPath: "/"
	},
	mode: 'development',
	module: {
		rules: [{
			exclude: /node_modules/,
			loader: require.resolve('babel-loader'),
			query: {
				presets: ['@babel/preset-env', '@babel/preset-react']
			}
		},
		{
			test: /\.css$/,
			use: [{
				loader: require.resolve('css-loader'),
				options: {}
			}]
		},
		{
			test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
			loader: require.resolve('url-loader'),
			options: {
				limit: 10000,
				name: 'static/media/[name].[hash:8].[ext]',
			},
		},
		{
			test: [/\.eot$/, /\.ttf$/, /\.svg$/, /\.woff$/, /\.woff2$/],
			loader: require.resolve('file-loader'),
			options: {
				name: 'static/media/[name].[hash:8].[ext]',
			},
		}]
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	plugins: [new webpack.HotModuleReplacementPlugin()],
	devServer: {
		hot: true,
		inline: false,
		historyApiFallback: true,
		contentBase: './',
		watchOptions: {
			aggregateTimeout: 300,
			poll: 1000
		}
	}
};
