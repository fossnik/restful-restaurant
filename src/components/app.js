import React, { Component } from 'react'
import { hot } from 'react-hot-loader'
import LandingPage from "./landing_page"

const App = class App extends Component {
	constructor() {
		super();

		this.state = {
			page: null
		};

		this.changePage = this.changePage.bind(this);
	}

	componentDidMount() {
		this.changePage({page: 'landing'});
	}

	changePage = (param) => {
		if (param.page === 'landing')
			this.setState({page: 'landing'});

		else
			throw new Error('Invalid State');
	};

	render() {
		switch (this.state.page) {
			case 'landing':
				return <LandingPage changePage={this.changePage}/>;

			case 'about':
				return <div>Not Yet Implemented</div>;

			default:
				return <div>Invalid State</div>;
		}
	}
}

export default hot(module)(App)
